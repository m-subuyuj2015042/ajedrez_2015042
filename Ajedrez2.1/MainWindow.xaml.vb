﻿Class MainWindow

    Private _matriz(0 To 7, 0 To 7) As Object

    Public Sub New()

        InitializeComponent()
        restarPieces()
    End Sub
    Private Sub restarPieces()
        Dim whitegray As New SolidColorBrush(Colors.LightGray)
        Dim gray As New SolidColorBrush(Colors.Gray)

        Dim blackColor As New SolidColorBrush(Colors.Black)
        Dim whiteColor As New SolidColorBrush(Colors.White)



        For y As Integer = 0 To 7
            For x As Integer = 0 To 7
                If (x Mod 2) Then
                    Dim blackSquare As New Rectangle
                    blackSquare.Fill = gray
                    If (y Mod 2) Then
                        Grid.SetRow(blackSquare, y)
                        Grid.SetColumn(blackSquare, x - 1)
                    Else
                        Grid.SetRow(blackSquare, y)
                        Grid.SetColumn(blackSquare, x)
                    End If
                    Tablero.Children.Add(blackSquare)
                Else
                    Dim whiteSquare As New Rectangle
                    whiteSquare.Fill = whitegray
                    If (y Mod 2) Then
                        Grid.SetRow(whiteSquare, y)
                        Grid.SetColumn(whiteSquare, x + 1)
                    Else
                        Grid.SetRow(whiteSquare, y)
                        Grid.SetColumn(whiteSquare, x)
                    End If
                    Tablero.Children.Add(whiteSquare)
                End If
            Next
        Next




        For x As Integer = 0 To 7
            Dim _blackCirclePro As New PawBlack
            Grid.SetColumn(_blackCirclePro, x)
            Grid.SetRow(_blackCirclePro, 1)
            Tablero.Children.Add(_blackCirclePro)
            _matriz(x, 1) = _blackCirclePro

            Dim _blackCirclePros As New PawnWhite
            Grid.SetColumn(_blackCirclePros, x)
            Grid.SetRow(_blackCirclePros, 6)
            Tablero.Children.Add(_blackCirclePros)
            _matriz(x, 6) = _blackCirclePros


        Next


        For x As Integer = 1 To 6 Step 5


            Dim _whiteCircle1 As New UserControl6
            Grid.SetColumn(_whiteCircle1, x)
            Grid.SetRow(_whiteCircle1, 7)
            Tablero.Children.Add(_whiteCircle1)
            _matriz(x, 7) = _whiteCircle1

            Dim _whiteCircle12 As New KnightBlack
            Grid.SetColumn(_whiteCircle12, x)
            Grid.SetRow(_whiteCircle12, 0)
            Tablero.Children.Add(_whiteCircle12)
            _matriz(x, 0) = _whiteCircle12


        Next

        For x As Integer = 0 To 7 Step 7



            Dim _whiteCircle1 As New RookWhite
            Grid.SetColumn(_whiteCircle1, x)
            Grid.SetRow(_whiteCircle1, 7)
            Tablero.Children.Add(_whiteCircle1)
            _matriz(x, 7) = _whiteCircle1

            Dim _whiteCircle12 As New UserControl1
            Grid.SetColumn(_whiteCircle12, x)
            Grid.SetRow(_whiteCircle12, 0)
            Tablero.Children.Add(_whiteCircle12)
            _matriz(x, 0) = _whiteCircle12


        Next

        For x As Integer = 2 To 5 Step 3


            Dim _whiteCircle1 As New BishpWhite
            Grid.SetColumn(_whiteCircle1, x)
            Grid.SetRow(_whiteCircle1, 7)
            Tablero.Children.Add(_whiteCircle1)
            _matriz(x, 7) = _whiteCircle1

            Dim _whiteCircle12 As New UserControl2
            Grid.SetColumn(_whiteCircle12, x)
            Grid.SetRow(_whiteCircle12, 0)
            Tablero.Children.Add(_whiteCircle12)
            _matriz(x, 0) = _whiteCircle12


        Next

        For x As Integer = 3 To 3



            Dim _whiteCircle1 As New QueenWhite
            Grid.SetColumn(_whiteCircle1, x)
            Grid.SetRow(_whiteCircle1, 7)
            Tablero.Children.Add(_whiteCircle1)
            _matriz(x, 7) = _whiteCircle1

            Dim _whiteCircle12 As New UserControl4
            Grid.SetColumn(_whiteCircle12, x)
            Grid.SetRow(_whiteCircle12, 0)
            Tablero.Children.Add(_whiteCircle12)
            _matriz(x, 0) = _whiteCircle12


        Next

        For x As Integer = 4 To 4

            Dim _whiteCircle1 As New UserControl5
            Grid.SetColumn(_whiteCircle1, x)
            Grid.SetRow(_whiteCircle1, 7)
            Tablero.Children.Add(_whiteCircle1)
            _matriz(x, 7) = _whiteCircle1

            Dim _whiteCircle12 As New UserControl3
            Grid.SetColumn(_whiteCircle12, x)
            Grid.SetRow(_whiteCircle12, 0)
            Tablero.Children.Add(_whiteCircle12)
            _matriz(x, 0) = _whiteCircle12


        Next

    End Sub

    Private Sub Window_SizeChanged(sender As Object, e As SizeChangedEventArgs)
        tagZ.Width = (e.NewSize.Width \ 20)
        tagA.Width = (e.NewSize.Width \ 13)
        tagB.Width = (e.NewSize.Width \ 13)
        tagC.Width = (e.NewSize.Width \ 13)
        tagD.Width = (e.NewSize.Width \ 13)
        tagE.Width = (e.NewSize.Width \ 13)
        tagF.Width = (e.NewSize.Width \ 13)
        tagG.Width = (e.NewSize.Width \ 13)
        tagH.Width = (e.NewSize.Width \ 13)

        tag9.Height = (e.NewSize.Height \ 9)
        tag1.Height = (e.NewSize.Height \ 10)
        tag2.Height = (e.NewSize.Height \ 10)
        tag3.Height = (e.NewSize.Height \ 10)
        tag4.Height = (e.NewSize.Height \ 10)
        tag5.Height = (e.NewSize.Height \ 10)
        tag6.Height = (e.NewSize.Height \ 10)
        tag7.Height = (e.NewSize.Height \ 10)
        tag8.Height = (e.NewSize.Height \ 10)

    End Sub

    Dim whitegray As New SolidColorBrush(Colors.LightGray)
    Dim gray As New SolidColorBrush(Colors.Gray)

    Dim blackColor As New SolidColorBrush(Colors.Black)
    Dim whiteColor As New SolidColorBrush(Colors.White)

    Dim _mouseX As Integer
    Dim _mouseY As Integer

    Dim _NubX As Integer
    Dim _NubY As Integer

    Private Sub Tablero_MouseMove(sender As Object, e As MouseEventArgs)
        Dim letra As String = ""
        Dim _mouseValue As String = e.GetPosition(sender).Y \ -75 + 8
        _mouseX = e.GetPosition(sender).X \ 75
        _mouseY = e.GetPosition(sender).Y \ 75
        Select Case _mouseX
            Case 0
                letra = "A"
            Case 1
                letra = "B"
            Case 2
                letra = "C"
            Case 3
                letra = "D"
            Case 4
                letra = "E"
            Case 5
                letra = "F"
            Case 6
                letra = "G"
            Case 7
                letra = "H"
        End Select

        Informacion.Text = "Posición: " & letra & "," & _mouseY & "; Turno: " & Turno & ""


    End Sub

    Dim _alternador As Boolean = True
    Dim _newX As Integer
    Dim _newY As Integer
    Dim objeto As Object
    Private _cuadrito(0 To 7, 0 To 7) As Object
    Dim marco As New CuadroSeleccion
    Dim Turno As String = ""

    Private Sub Tablero_MouseDown(sender As Object, e As MouseButtonEventArgs)
        If _alternador Then
            Turno = "Negras"
        Else
            Turno = "Blancas"
        End If
        Try
            If _alternador Then
                Try
                    If IsNothing(objeto) Then
                        Select Case _matriz(_mouseX, _mouseY).ToString
                            Case "Ajedrez2._1.PawnWhite"
                                objeto = _matriz(_mouseX, _mouseY)
                                _newX = _mouseX
                                _newY = _mouseY
                                AgregarCuadrito()
                            Case "Ajedrez2._1.UserControl5"
                                objeto = _matriz(_mouseX, _mouseY)
                                _newX = _mouseX
                                _newY = _mouseY
                                AgregarCuadrito()
                            Case "Ajedrez2._1.QueenWhite"
                                objeto = _matriz(_mouseX, _mouseY)
                                _newX = _mouseX
                                _newY = _mouseY
                                AgregarCuadrito()
                            Case "Ajedrez2._1.BishpWhite"
                                objeto = _matriz(_mouseX, _mouseY)
                                _newX = _mouseX
                                _newY = _mouseY
                                AgregarCuadrito()
                            Case "Ajedrez2._1.RookWhite"
                                objeto = _matriz(_mouseX, _mouseY)
                                _newX = _mouseX
                                _newY = _mouseY
                                AgregarCuadrito()
                            Case "Ajedrez2._1.UserControl6"
                                objeto = _matriz(_mouseX, _mouseY)
                                _newX = _mouseX
                                _newY = _mouseY
                                AgregarCuadrito()
                        End Select

                    ElseIf Not IsNothing(objeto) Then
                        Select Case objeto.ToString
                            Case "Ajedrez2._1.PawnWhite"
                                Dim vacio As Object
                                vacio = _matriz(_mouseX, _mouseY.ToString)
                                If Not IsNothing(vacio) Then

                                    If _newY - _mouseY = 1 And _newX - _mouseX = 1 Or _newY - _mouseY = 1 And _mouseX - _newX = 1 Then
                                        If _IsBlack(vacio) Then
                                            Try
                                                Comer()
                                                Mover()
                                                LimpiarCuadrito()
                                                Coronar()
                                                _alternador = False
                                            Catch ex As Exception
                                            End Try
                                        Else
                                            Deselecciona()
                                            LimpiarCuadrito()
                                        End If
                                    Else
                                        Deselecciona()
                                        LimpiarCuadrito()

                                    End If

                                Else
                                    If _newY - _mouseY = 1 And _mouseX = _newX Then
                                        Mover()
                                        LimpiarCuadrito()
                                        Coronar()
                                        _alternador = False
                                    ElseIf _newY - _mouseY = 2 And _newY = 6 And _mouseX = _newX Then
                                        If IsNothing(_matriz(_mouseX, 5.ToString)) Then
                                            Mover()
                                            LimpiarCuadrito()
                                            _alternador = False
                                        Else
                                            Deselecciona()
                                            LimpiarCuadrito()
                                        End If
                                    Else
                                        Deselecciona()
                                        LimpiarCuadrito()
                                    End If
                                End If
                            Case "Ajedrez2._1.RookWhite"
                                MoveTorre()
                            Case "Ajedrez2._1.BishpWhite"
                                MoveAlfil()
                            Case "Ajedrez2._1.QueenWhite"
                                Reina()
                            Case "Ajedrez2._1.UserControl6"
                                MoveCaballo()
                            Case "Ajedrez2._1.UserControl5"
                                MoveKing()
                        End Select

                    End If
                Catch ex As Exception

                End Try

            Else
                Try
                    If IsNothing(objeto) Then
                        Select Case _matriz(_mouseX, _mouseY).ToString
                            Case "Ajedrez2._1.PawBlack"
                                objeto = _matriz(_mouseX, _mouseY)
                                _newX = _mouseX
                                _newY = _mouseY
                                AgregarCuadrito()
                            Case "Ajedrez2._1.UserControl3"
                                objeto = _matriz(_mouseX, _mouseY)
                                _newX = _mouseX
                                _newY = _mouseY
                                AgregarCuadrito()
                            Case "Ajedrez2._1.UserControl4"
                                objeto = _matriz(_mouseX, _mouseY)
                                _newX = _mouseX
                                _newY = _mouseY
                                AgregarCuadrito()
                            Case "Ajedrez2._1.UserControl2"
                                objeto = _matriz(_mouseX, _mouseY)
                                _newX = _mouseX
                                _newY = _mouseY
                                AgregarCuadrito()
                            Case "Ajedrez2._1.UserControl1"
                                objeto = _matriz(_mouseX, _mouseY)
                                _newX = _mouseX
                                _newY = _mouseY
                                AgregarCuadrito()
                            Case "Ajedrez2._1.KnightBlack"
                                objeto = _matriz(_mouseX, _mouseY)
                                _newX = _mouseX
                                _newY = _mouseY
                                AgregarCuadrito()
                        End Select

                    ElseIf Not IsNothing(objeto) Then
                        Select Case objeto.ToString
                            Case "Ajedrez2._1.PawBlack"
                                Dim vacio As Object
                                vacio = _matriz(_mouseX, _mouseY.ToString)
                                If Not IsNothing(vacio) Then

                                    If _mouseY - _newY = 1 And Math.Abs(_newX - _mouseX) = 1 Then
                                        If Not _IsBlack(vacio) Then
                                            Try
                                                Comer()
                                                Mover()
                                                LimpiarCuadrito()
                                                _alternador = True
                                            Catch ex As Exception
                                            End Try
                                        Else
                                            Deselecciona()
                                            LimpiarCuadrito()
                                        End If
                                    Else
                                        Deselecciona()
                                        LimpiarCuadrito()

                                    End If

                                Else
                                    If _mouseY - _newY = 1 And _mouseX = _newX Then
                                        Mover()
                                        _alternador = True
                                        LimpiarCuadrito()
                                    ElseIf _mouseY - _newY = 2 And _newY = 1 And _mouseX = _newX Then
                                        If IsNothing(_matriz(_mouseX, 2.ToString)) Then
                                            Mover()
                                            _alternador = True
                                            LimpiarCuadrito()
                                        Else
                                            Deselecciona()
                                            LimpiarCuadrito()
                                        End If
                                    Else
                                        Deselecciona()
                                        LimpiarCuadrito()
                                    End If
                                End If

                            Case "Ajedrez2._1.UserControl3"
                                MoveKing()

                            Case "Ajedrez2._1.KnightBlack"
                                MoveCaballo()

                            Case "Ajedrez2._1.UserControl2"
                                MoveAlfil()

                            Case "Ajedrez2._1.UserControl1"
                                MoveTorre()

                            Case "Ajedrez2._1.UserControl4"
                                Reina()
                        End Select
                    End If
                Catch ex As Exception

                End Try
            End If
        Catch ex As Exception
            MsgBox("Turno equivocado")
        End Try

    End Sub

    Public Sub Mover()
        Tablero.Children.Remove(_matriz(_newX, _newY))
        _matriz(_newX, _newY) = Nothing
        Grid.SetColumn(objeto, _mouseX)
        Grid.SetRow(objeto, _mouseY)
        Tablero.Children.Add(objeto)
        _matriz(_mouseX, _mouseY) = objeto
        objeto = Nothing

    End Sub

    Public Sub LimpiarCuadrito()
        Tablero.Children.Remove(_cuadrito(_newX, _newY))

    End Sub

    Public Sub Deselecciona()
        _matriz(_newX, _newY) = objeto
        objeto = Nothing

    End Sub

    Public Sub Comer()
        Tablero.Children.Remove(_matriz(_mouseX, _mouseY))
        _matriz(_mouseX, _mouseY) = Nothing
    End Sub

    Public Sub AgregarCuadrito()
        Grid.SetColumn(marco, _mouseX)
        Grid.SetRow(marco, _mouseY)
        Tablero.Children.Add(marco)
        _cuadrito(_mouseX, _mouseY) = marco
    End Sub

    Public Sub MoveKing()
        Dim vacio As Object
        vacio = _matriz(_mouseX, _mouseY.ToString)
        If Not IsNothing(vacio) Then
            If Math.Abs(_newY - _mouseY) = 1 And Math.Abs(_newX - _mouseX) = 1 Or Math.Abs(_newY - _mouseY) = 1 And _newX = _mouseX _
                Or Math.Abs(_newX - _mouseX) = 1 And _newY = _mouseY Then
                If Equals(_matriz(_newX, _newY).ToString, "Ajedrez2._1.UserControl5") Then
                    If _IsBlack(vacio) Then
                        Try
                            Comer()
                            Mover()
                            LimpiarCuadrito()
                            _alternador = False
                        Catch ex As Exception
                        End Try
                    Else
                        Deselecciona()
                        LimpiarCuadrito()
                    End If
                ElseIf Equals(_matriz(_newX, _newY).ToString, "Ajedrez2._1.UserControl3") Then
                    If Not _IsBlack(vacio) Then
                        Try
                            Comer()
                            Mover()
                            LimpiarCuadrito()
                            _alternador = True
                        Catch ex As Exception
                        End Try
                    Else
                        Deselecciona()
                        LimpiarCuadrito()
                    End If
                End If
            Else
                Deselecciona()
                LimpiarCuadrito()

            End If

        Else
            If Equals(_matriz(_newX, _newY).ToString, "Ajedrez2._1.UserControl5") Then
                If Math.Abs(_newY - _mouseY) = 1 And Math.Abs(_newX - _mouseX) = 1 Or Math.Abs(_newY - _mouseY) = 1 And _newX = _mouseX _
                Or Math.Abs(_newX - _mouseX) = 1 And _newY = _mouseY Then
                    Mover()
                    LimpiarCuadrito()
                    _alternador = False
                Else
                    LimpiarCuadrito()
                    Deselecciona()
                End If
            ElseIf Equals(_matriz(_newX, _newY).ToString, "Ajedrez2._1.UserControl3") Then
                If Math.Abs(_newY - _mouseY) = 1 And Math.Abs(_newX - _mouseX) = 1 Or Math.Abs(_newY - _mouseY) = 1 And _newX = _mouseX _
                Or Math.Abs(_newX - _mouseX) = 1 And _newY = _mouseY Then
                    Mover()
                    LimpiarCuadrito()
                    _alternador = True
                Else
                    LimpiarCuadrito()
                    Deselecciona()
                End If
            End If
        End If
    End Sub

    Public Sub MoveAlfil()
        If Math.Abs(_newX - _mouseX) = Math.Abs(_newY - _mouseY) Then
            If _newX - _mouseX < 0 And _newY - _mouseY > 0 Then

                Dim verificar As Boolean = True
                For Y As Integer = _newY - 1 To _mouseY + 1 Step -1
                    For X As Integer = _newX + 1 To _mouseX - 1 Step +1

                        Dim espacio As Object
                        espacio = _matriz(X, Y.ToString)
                        Y = Y - 1

                        If Not IsNothing(espacio) Then
                            verificar = False
                            Exit For

                        End If
                    Next

                    If verificar = False Then
                        Exit For
                    End If
                Next

                If verificar = True Then
                    Dim Vacio As Object
                    Vacio = _matriz(_mouseX, _mouseY.ToString)
                    If Equals(_matriz(_newX, _newY).ToString, "Ajedrez2._1.BishpWhite") Or Equals(_matriz(_newX, _newY).ToString, "Ajedrez2._1.QueenWhite") Then
                        If Not IsNothing(Vacio) Then

                            If _IsBlack(Vacio) Then
                                Try
                                    Comer()
                                    Mover()
                                    LimpiarCuadrito()
                                    _alternador = False
                                Catch ex As Exception
                                End Try
                            Else
                                Deselecciona()
                                LimpiarCuadrito()
                            End If
                        Else
                            Mover()
                            _alternador = False
                            LimpiarCuadrito()
                        End If
                    ElseIf Equals(_matriz(_newX, _newY).ToString, "Ajedrez2._1.UserControl2") Or Equals(_matriz(_newX, _newY).ToString, "Ajedrez2._1.UserControl4") Then
                        If Not IsNothing(Vacio) Then

                            If Not _IsBlack(Vacio) Then
                                Try
                                    Comer()
                                    Mover()
                                    LimpiarCuadrito()
                                    _alternador = True
                                Catch ex As Exception
                                End Try
                            Else
                                Deselecciona()
                                LimpiarCuadrito()
                            End If
                        Else
                            Mover()
                            _alternador = True
                            LimpiarCuadrito()
                        End If
                    End If

                ElseIf verificar = False Then
                    Deselecciona()
                    LimpiarCuadrito()

                End If

            ElseIf _newX - _mouseX > 0 And _newY - _mouseY > 0 Then

                Dim verificar As Boolean = True
                For Y As Integer = _newY - 1 To _mouseY + 1 Step -1
                    For X As Integer = _newX - 1 To _mouseX + 1 Step -1

                        Dim espacio As Object
                        espacio = _matriz(X, Y.ToString)
                        Y = Y - 1

                        If Not IsNothing(espacio) Then
                            verificar = False
                            Exit For

                        End If
                    Next

                    If verificar = False Then
                        Exit For
                    End If
                Next

                If verificar = True Then
                    Dim Vacio As Object
                    Vacio = _matriz(_mouseX, _mouseY.ToString)
                    If Not IsNothing(Vacio) Then
                        If Equals(_matriz(_newX, _newY).ToString, "Ajedrez2._1.BishpWhite") Or Equals(_matriz(_newX, _newY).ToString, "Ajedrez2._1.QueenWhite") Then
                            If _IsBlack(Vacio) Then
                                Try
                                    Comer()
                                    Mover()
                                    LimpiarCuadrito()
                                    _alternador = False
                                Catch ex As Exception
                                End Try
                            Else
                                Deselecciona()
                                LimpiarCuadrito()
                            End If
                        ElseIf Equals(_matriz(_newX, _newY).ToString, "Ajedrez2._1.UserControl2") Or Equals(_matriz(_newX, _newY).ToString, "Ajedrez2._1.UserControl4") Then
                            If Not _IsBlack(Vacio) Then
                                Try
                                    Comer()
                                    Mover()
                                    LimpiarCuadrito()
                                    _alternador = True
                                Catch ex As Exception
                                End Try
                            End If
                        End If
                    Else
                        If Equals(_matriz(_newX, _newY).ToString, "Ajedrez2._1.BishpWhite") Or Equals(_matriz(_newX, _newY).ToString, "Ajedrez2._1.QueenWhite") Then
                            Mover()
                            _alternador = False
                            LimpiarCuadrito()
                        ElseIf Equals(_matriz(_newX, _newY).ToString, "Ajedrez2._1.UserControl2") Or Equals(_matriz(_newX, _newY).ToString, "Ajedrez2._1.UserControl4") Then
                            Mover()
                            _alternador = True
                            LimpiarCuadrito()
                        End If

                    End If


                ElseIf verificar = False Then
                    Deselecciona()
                    LimpiarCuadrito()

                End If

            ElseIf _newX - _mouseX < 0 And _newY - _mouseY < 0 Then

                Dim verificar As Boolean = True
                For Y As Integer = _newY + 1 To _mouseY - 1 Step +1
                    For X As Integer = _newX + 1 To _mouseX - 1 Step +1

                        Dim espacio As Object
                        espacio = _matriz(X, Y.ToString)
                        Y = Y + 1

                        If Not IsNothing(espacio) Then
                            verificar = False
                            Exit For

                        End If
                    Next

                    If verificar = False Then
                        Exit For
                    End If
                Next

                If verificar = True Then
                    Dim Vacio As Object
                    Vacio = _matriz(_mouseX, _mouseY.ToString)
                    If Not IsNothing(Vacio) Then
                        If Equals(_matriz(_newX, _newY).ToString, "Ajedrez2._1.BishpWhite") Or Equals(_matriz(_newX, _newY).ToString, "Ajedrez2._1.QueenWhite") Then
                            If _IsBlack(Vacio) Then
                                Try
                                    Comer()
                                    Mover()
                                    LimpiarCuadrito()
                                    _alternador = False
                                Catch ex As Exception
                                End Try
                            Else
                                Deselecciona()
                                LimpiarCuadrito()
                            End If
                        ElseIf Equals(_matriz(_newX, _newY).ToString, "Ajedrez2._1.UserControl2") Or Equals(_matriz(_newX, _newY).ToString, "Ajedrez2._1.UserControl4") Then
                            If Not _IsBlack(Vacio) Then
                                Try
                                    Comer()
                                    Mover()
                                    LimpiarCuadrito()
                                    _alternador = True
                                Catch ex As Exception
                                End Try
                            End If
                        End If
                    Else
                        If Equals(_matriz(_newX, _newY).ToString, "Ajedrez2._1.BishpWhite") Or Equals(_matriz(_newX, _newY).ToString, "Ajedrez2._1.QueenWhite") Then
                            Mover()
                            _alternador = False
                            LimpiarCuadrito()
                        ElseIf Equals(_matriz(_newX, _newY).ToString, "Ajedrez2._1.UserControl2") Or Equals(_matriz(_newX, _newY).ToString, "Ajedrez2._1.UserControl4") Then
                            Mover()
                            _alternador = True
                            LimpiarCuadrito()
                        End If
                    End If


                ElseIf verificar = False Then
                    Deselecciona()
                    LimpiarCuadrito()

                End If

            ElseIf _newX - _mouseX > 0 And _newY - _mouseY < 0 Then

                Dim verificar As Boolean = True
                For Y As Integer = _newY + 1 To _mouseY - 1 Step +1
                    For X As Integer = _newX - 1 To _mouseX + 1 Step -1

                        Dim espacio As Object
                        espacio = _matriz(X, Y.ToString)
                        Y = Y + 1

                        If Not IsNothing(espacio) Then
                            verificar = False
                            Exit For

                        End If
                    Next

                    If verificar = False Then
                        Exit For
                    End If
                Next

                If verificar = True Then
                    Dim Vacio As Object
                    Vacio = _matriz(_mouseX, _mouseY.ToString)
                    If Not IsNothing(Vacio) Then
                        If Equals(_matriz(_newX, _newY).ToString, "Ajedrez2._1.BishpWhite") Or Equals(_matriz(_newX, _newY).ToString, "Ajedrez2._1.QueenWhite") Then
                            If _IsBlack(Vacio) Then
                                Try
                                    Comer()
                                    Mover()
                                    LimpiarCuadrito()
                                    _alternador = False
                                Catch ex As Exception
                                End Try
                            Else
                                Deselecciona()
                                LimpiarCuadrito()
                            End If
                        ElseIf Equals(_matriz(_newX, _newY).ToString, "Ajedrez2._1.UserControl2") Or Equals(_matriz(_newX, _newY).ToString, "Ajedrez2._1.UserControl4") Then
                            If Not _IsBlack(Vacio) Then
                                Try
                                    Comer()
                                    Mover()
                                    LimpiarCuadrito()
                                    _alternador = True
                                Catch ex As Exception
                                End Try
                            End If
                        End If
                    Else
                        If Equals(_matriz(_newX, _newY).ToString, "Ajedrez2._1.BishpWhite") Or Equals(_matriz(_newX, _newY).ToString, "Ajedrez2._1.QueenWhite") Then
                            Mover()
                            _alternador = False
                            LimpiarCuadrito()
                        ElseIf Equals(_matriz(_newX, _newY).ToString, "Ajedrez2._1.UserControl2") Or Equals(_matriz(_newX, _newY).ToString, "Ajedrez2._1.UserControl4") Then
                            Mover()
                            _alternador = True
                            LimpiarCuadrito()
                        End If
                    End If


                ElseIf verificar = False Then
                    Deselecciona()
                    LimpiarCuadrito()

                End If
            Else
                MsgBox("Movimiento erroneo")
                Deselecciona()
                LimpiarCuadrito()
            End If
        Else
            MsgBox("Movimiento erroneo")
            Deselecciona()
        End If
    End Sub

    Public Sub MoveTorre()
        If _newX = _mouseX Or _newY = _mouseY Then

            If _mouseX = _newX Then

                If _newY - _mouseY > 0 Then

                    Dim verificar As Boolean = True

                    For y As Integer = _newY - 1 To _mouseY + 1 Step -1
                        Dim espacio As Object
                        espacio = _matriz(_newX, y.ToString)


                        If Not IsNothing(espacio) Then
                            verificar = False
                            Exit For

                        End If

                    Next

                    If verificar = True Then
                        Dim Vacio As Object
                        Vacio = _matriz(_mouseX, _mouseY.ToString)
                        If Not IsNothing(Vacio) Then
                            If Equals(_matriz(_newX, _newY).ToString, "Ajedrez2._1.RookWhite") Or Equals(_matriz(_newX, _newY).ToString, "Ajedrez2._1.QueenWhite") Then
                                If _IsBlack(Vacio) Then
                                    Try
                                        Comer()
                                        Mover()
                                        LimpiarCuadrito()
                                        _alternador = False
                                    Catch ex As Exception
                                    End Try
                                Else
                                    Deselecciona()
                                    LimpiarCuadrito()
                                End If
                            ElseIf Equals(_matriz(_newX, _newY).ToString, "Ajedrez2._1.UserControl1") Or Equals(_matriz(_newX, _newY).ToString, "Ajedrez2._1.UserControl4") Then
                                If Not _IsBlack(Vacio) Then
                                    Try
                                        Comer()
                                        Mover()
                                        LimpiarCuadrito()
                                        _alternador = True
                                    Catch ex As Exception
                                    End Try
                                Else
                                    Deselecciona()
                                    LimpiarCuadrito()
                                End If
                            End If
                        Else
                            If Equals(_matriz(_newX, _newY).ToString, "Ajedrez2._1.RookWhite") Or Equals(_matriz(_newX, _newY).ToString, "Ajedrez2._1.QueenWhite") Then
                                Mover()
                                _alternador = False
                                LimpiarCuadrito()
                            ElseIf Equals(_matriz(_newX, _newY).ToString, "Ajedrez2._1.UserControl1") Or Equals(_matriz(_newX, _newY).ToString, "Ajedrez2._1.UserControl4") Then
                                Mover()
                                _alternador = True
                                LimpiarCuadrito()
                            End If
                        End If


                    ElseIf verificar = False Then
                        Deselecciona()
                        LimpiarCuadrito()

                    End If

                ElseIf _newY - _mouseY < 0 Then
                    Dim verificar As Boolean = True
                    For y As Integer = _newY + 1 To _mouseY - 1 Step +1
                        Dim espacio As Object
                        espacio = _matriz(_newX, y.ToString)
                        If Not IsNothing(espacio) Then
                            verificar = False
                            Exit For
                        End If
                    Next
                    If verificar = True Then
                        Dim Vacio As Object
                        Vacio = _matriz(_mouseX, _mouseY.ToString)
                        If Not IsNothing(Vacio) Then
                            If Equals(_matriz(_newX, _newY).ToString, "Ajedrez2._1.RookWhite") Or Equals(_matriz(_newX, _newY).ToString, "Ajedrez2._1.QueenWhite") Then
                                If _IsBlack(Vacio) Then
                                    Try
                                        Comer()
                                        Mover()
                                        LimpiarCuadrito()
                                        _alternador = False
                                    Catch ex As Exception
                                    End Try
                                Else
                                    Deselecciona()
                                    LimpiarCuadrito()
                                End If
                            ElseIf Equals(_matriz(_newX, _newY).ToString, "Ajedrez2._1.UserControl1") Or Equals(_matriz(_newX, _newY).ToString, "Ajedrez2._1.UserControl4") Then
                                If Not _IsBlack(Vacio) Then
                                    Try
                                        Comer()
                                        Mover()
                                        LimpiarCuadrito()
                                        _alternador = True
                                    Catch ex As Exception
                                    End Try
                                Else
                                    Deselecciona()
                                    LimpiarCuadrito()
                                End If
                            End If
                        Else
                            If Equals(_matriz(_newX, _newY).ToString, "Ajedrez2._1.RookWhite") Or Equals(_matriz(_newX, _newY).ToString, "Ajedrez2._1.QueenWhite") Then
                                Mover()
                                _alternador = False
                                LimpiarCuadrito()
                            ElseIf Equals(_matriz(_newX, _newY).ToString, "Ajedrez2._1.UserControl1") Or Equals(_matriz(_newX, _newY).ToString, "Ajedrez2._1.UserControl4") Then
                                Mover()
                                _alternador = True
                                LimpiarCuadrito()
                            End If
                        End If

                    ElseIf verificar = False Then
                        Deselecciona()
                        LimpiarCuadrito()
                    End If

                End If
            Else
                If _newX - _mouseX < 0 Then
                    Dim verificar As Boolean = True
                    For x As Integer = _newX + 1 To _mouseX - 1 Step 1
                        Dim espacio As Object
                        espacio = _matriz(x, _newY.ToString)
                        If Not IsNothing(espacio) Then
                            verificar = False
                            Exit For
                        End If
                    Next

                    If verificar = True Then
                        Dim Vacio As Object
                        Vacio = _matriz(_mouseX, _mouseY.ToString)
                        If Not IsNothing(Vacio) Then
                            If Equals(_matriz(_newX, _newY).ToString, "Ajedrez2._1.RookWhite") Or Equals(_matriz(_newX, _newY).ToString, "Ajedrez2._1.QueenWhite") Then
                                If _IsBlack(Vacio) Then
                                    Try
                                        Comer()
                                        Mover()
                                        LimpiarCuadrito()
                                        _alternador = False
                                    Catch ex As Exception
                                    End Try
                                Else
                                    Deselecciona()
                                    LimpiarCuadrito()
                                End If
                            ElseIf Equals(_matriz(_newX, _newY).ToString, "Ajedrez2._1.UserControl1") Or Equals(_matriz(_newX, _newY).ToString, "Ajedrez2._1.UserControl4") Then
                                If Not _IsBlack(Vacio) Then
                                    Try
                                        Comer()
                                        Mover()
                                        LimpiarCuadrito()
                                        _alternador = True
                                    Catch ex As Exception
                                    End Try
                                Else
                                    Deselecciona()
                                    LimpiarCuadrito()
                                End If
                            End If
                        Else
                            If Equals(_matriz(_newX, _newY).ToString, "Ajedrez2._1.RookWhite") Or Equals(_matriz(_newX, _newY).ToString, "Ajedrez2._1.QueenWhite") Then
                                Mover()
                                _alternador = False
                                LimpiarCuadrito()
                            ElseIf Equals(_matriz(_newX, _newY).ToString, "Ajedrez2._1.UserControl1") Or Equals(_matriz(_newX, _newY).ToString, "Ajedrez2._1.UserControl4") Then
                                Mover()
                                _alternador = True
                                LimpiarCuadrito()
                            End If
                        End If

                    ElseIf verificar = False Then
                        Deselecciona()
                        LimpiarCuadrito()
                    End If

                ElseIf _newX - _mouseX > 0 Then

                    Dim verificar As Boolean = True

                    For x As Integer = _newX - 1 To _mouseX + 1 Step -1
                        Dim espacio As Object
                        espacio = _matriz(x, _newY.ToString)
                        If Not IsNothing(espacio) Then
                            verificar = False
                            Exit For
                        End If

                    Next

                    If verificar = True Then
                        Dim Vacio As Object
                        Vacio = _matriz(_mouseX, _mouseY.ToString)
                        If Not IsNothing(Vacio) Then
                            If Equals(_matriz(_newX, _newY).ToString, "Ajedrez2._1.RookWhite") Or Equals(_matriz(_newX, _newY).ToString, "Ajedrez2._1.QueenWhite") Then
                                If _IsBlack(Vacio) Then
                                    Try
                                        Comer()
                                        Mover()
                                        LimpiarCuadrito()
                                        _alternador = False
                                    Catch ex As Exception
                                    End Try
                                Else
                                    Deselecciona()
                                    LimpiarCuadrito()
                                End If
                            ElseIf Equals(_matriz(_newX, _newY).ToString, "Ajedrez2._1.UserControl1") Or Equals(_matriz(_newX, _newY).ToString, "Ajedrez2._1.UserControl4") Then
                                If Not _IsBlack(Vacio) Then
                                    Try
                                        Comer()
                                        Mover()
                                        LimpiarCuadrito()
                                        _alternador = True
                                    Catch ex As Exception
                                    End Try
                                Else
                                    Deselecciona()
                                    LimpiarCuadrito()
                                End If
                            End If
                        Else
                            If Equals(_matriz(_newX, _newY).ToString, "Ajedrez2._1.RookWhite") Or Equals(_matriz(_newX, _newY).ToString, "Ajedrez2._1.QueenWhite") Then
                                Mover()
                                _alternador = False
                                LimpiarCuadrito()
                            ElseIf Equals(_matriz(_newX, _newY).ToString, "Ajedrez2._1.UserControl1") Or Equals(_matriz(_newX, _newY).ToString, "Ajedrez2._1.UserControl4") Then
                                Mover()
                                _alternador = True
                                LimpiarCuadrito()
                            End If
                        End If

                    ElseIf verificar = False Then
                        Deselecciona()
                        LimpiarCuadrito()
                    End If

                End If
            End If

        Else
            Deselecciona()
        End If
    End Sub

    Public Sub Reina()
        If _newX = _mouseX Or _newY = _mouseY Then
            MoveTorre()
        ElseIf Math.Abs(_newX - _mouseX) = Math.Abs(_newY - _mouseY) Then
            MoveAlfil()
        Else
            Deselecciona()
        End If
    End Sub

    Public Sub Coronar()
        If _mouseY = 0 Then
            Dim coronoarWindows As New Coronar
            coronoarWindows.Show()
        End If
    End Sub

    Public Sub MoveCaballo()
        Dim vacio As Object
        vacio = _matriz(_mouseX, _mouseY.ToString)
        If Not IsNothing(vacio) Then
            If Math.Abs(_newY - _mouseY) = 2 And Math.Abs(_newX - _mouseX) = 1 _
                Or Math.Abs(_newY - _mouseY) = 1 And Math.Abs(_newX - _mouseX) = 2 Then
                If Equals(_matriz(_newX, _newY).ToString, "Ajedrez2._1.UserControl6") Then
                    If _IsBlack(vacio) Then
                        Try
                            Comer()
                            Mover()
                            LimpiarCuadrito()
                            _alternador = False
                        Catch ex As Exception
                        End Try
                    Else
                        Deselecciona()
                        LimpiarCuadrito()
                    End If
                ElseIf Equals(_matriz(_newX, _newY).ToString, "Ajedrez2._1.KnightBlack") Then
                    If Not _IsBlack(vacio) Then
                        Try
                            Comer()
                            Mover()
                            LimpiarCuadrito()
                            _alternador = True
                        Catch ex As Exception
                        End Try
                    Else
                        Deselecciona()
                        LimpiarCuadrito()
                    End If
                End If
            Else
                Deselecciona()
                LimpiarCuadrito()
            End If
        Else
            If Equals(_matriz(_newX, _newY).ToString, "Ajedrez2._1.UserControl6") Then
                If Math.Abs(_newY - _mouseY) = 2 And Math.Abs(_newX - _mouseX) = 1 _
                Or Math.Abs(_newY - _mouseY) = 1 And Math.Abs(_newX - _mouseX) = 2 Then
                    Mover()
                    LimpiarCuadrito()
                    _alternador = False
                Else
                    Deselecciona()
                    LimpiarCuadrito()
                End If
            ElseIf Equals(_matriz(_newX, _newY).ToString, "Ajedrez2._1.KnightBlack") Then
                If Math.Abs(_newY - _mouseY) = 2 And Math.Abs(_newX - _mouseX) = 1 _
                Or Math.Abs(_newY - _mouseY) = 1 And Math.Abs(_newX - _mouseX) = 2 Then
                    Mover()
                    LimpiarCuadrito()
                    _alternador = True
                Else
                    Deselecciona()
                    LimpiarCuadrito()
                End If
            End If
        End If
    End Sub

    Public Function _IsBlack(vacio) As Boolean
        If Equals(vacio.ToString, "Ajedrez2._1.PawBlack") Or Equals(vacio.ToString, "Ajedrez2._1.UserControl3") _
                            Or Equals(vacio.ToString, "Ajedrez2._1.UserControl4") Or Equals(vacio.ToString, "Ajedrez2._1.UserControl2") _
                            Or Equals(vacio.ToString, "Ajedrez2._1.UserControl1") Or Equals(vacio.ToString, "Ajedrez2._1.KnightBlack") Then
            _IsBlack = True
        Else
            _IsBlack = False
        End If

    End Function

    Private Sub MenuItem_Click(sender As Object, e As RoutedEventArgs)
        Tablero.Children.Clear()
        For x As Integer = 0 To 7
            For y As Integer = 0 To 7
                _matriz(x, y) = Nothing
            Next
        Next
        _alternador = True
        restarPieces()
    End Sub

End Class
